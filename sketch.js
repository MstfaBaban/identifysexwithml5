let audioContext;
let mic;
let pitch;
let onePerson;
let a = true;
let male = 0;
let female = 0;
async function setup() {
  audioContext = new AudioContext();
  stream = await navigator.mediaDevices.getUserMedia({
    audio: true,
    video: false
  });
  audioContext.resume().then(() => {
    startPitch(stream, audioContext);
  });
}

function startPitch(stream, audioContext) {
  pitch = ml5.pitchDetection("./model/", audioContext, stream, modelLoaded);
}

function modelLoaded() {
  // document.querySelector("#status").textContent = "Model Loaded";
  // getPitch();
}

function start() {
  document.querySelector("#result").textContent = "Recording";

  document.querySelector("#status").textContent = "Model Loaded";

  getPitch();
}

function getPitch() {
  if (a) {
    pitch.getPitch(function(err, frequency) {
      // console.log(frequency);
      if (frequency > 170 && frequency != null) {
        // document.querySelector("#result").textContent = "female";
        document.querySelector("#result").textContent = "Detecting";

        female++;
      } else if (frequency < 170 && frequency != null) {
        // document.querySelector("#result").textContent = "male";
        document.querySelector("#result").textContent = "Detecting";

        male++;
      } else {
        document.querySelector("#result").textContent = "No pitch detected";
      }
      getPitch();
    });

    setTimeout(function() {
      a = false;
      if (male > female) {
        document.querySelector("#result").textContent = "male";
      } else {
        document.querySelector("#result").textContent = "female";
      }
    }, 6000);
  }
}
